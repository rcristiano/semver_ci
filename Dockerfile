FROM python:3.8.1-alpine3.11

ENV POETRY_HOME="/etc/poetry" \
    POETRY_VIRTUALENVS_CREATE="false" \
    PATH="/etc/poetry/bin:$PATH"

RUN apk add curl && \
    curl -sSL https://raw.githubusercontent.com/python-poetry/poetry/master/get-poetry.py \
    | python && \
    apk del curl

COPY pyproject.toml .

RUN poetry install --no-dev

COPY app/ .

CMD [ "python", "hello.py" ]